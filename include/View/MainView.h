#ifndef _main_view_class_
#define _main_view_class_

#include <gtkmm.h>
#include "View/MyWidget.h"

namespace View
{
	class MainView : public Gtk::Window
	{

		public:
			MainView();
			virtual ~MainView();

		protected:
			// Signal handlers
			void on_button_quit();

			// Child widgets
			Gtk::Box m_VBox;
			MyWidget m_MyWidget;
			Gtk::ButtonBox m_ButtonBox;
			Gtk::Button m_Button_Quit;

	};
}

#endif
