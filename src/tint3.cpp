#include "View/MainView.h"

#include <gtkmm/application.h>

int main(int argc, char * argv[])
{

	auto app = Gtk::Application::create(argc, argv, "org.degva.tint3");

	/*
	View::MainView * mv = new View::MainView();
	mv->CreateWindow();
	mv->ShowWindow();
	*/

	View::MainView view;

	return app->run(view);
}
