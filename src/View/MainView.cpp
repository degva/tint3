
#include "View/MainView.h"

using namespace View;

/**
 * MainView Constructor
 */
MainView::MainView() : 
	m_VBox(Gtk::ORIENTATION_VERTICAL),
	m_Button_Quit("Quit")
{
	set_title("Test");
	set_border_width(6);
	set_default_size(400, 200);

	add(m_VBox);

	m_VBox.pack_start(m_MyWidget, Gtk::PACK_EXPAND_WIDGET);
	m_MyWidget.show();

	m_VBox.pack_start(m_ButtonBox, Gtk::PACK_SHRINK);
	m_ButtonBox.pack_start(m_Button_Quit, Gtk::PACK_SHRINK);
	m_ButtonBox.set_layout(Gtk::BUTTONBOX_END);
	m_Button_Quit.signal_clicked().connect( sigc::mem_fun(*this, &MainView::on_button_quit ) );

	show_all_children();
}

/**
 * MainView Destructor
 */
MainView::~MainView()
{
	this->m_MyWidget.~MyWidget();
}

void MainView::on_button_quit()
{
	hide();
}
